console.log("JavaScript is a versatile programming language widely utilized across diverse areas of software development. Initially recognized for enhancing web page interactivity, it has evolved to power modern web applications through frameworks like React.js and Angular, while also enabling server-side development with Node.js. Beyond the web, JavaScript supports mobile app development, desktop applications, IoT programming, game development, data visualization, and web browser extensions. Its broad ecosystem of libraries and frameworks empowers developers to innovate and create feature-rich applications efficiently across various platforms.")
let a = 10
let b = 20
let sum = a+b
console.log(sum)